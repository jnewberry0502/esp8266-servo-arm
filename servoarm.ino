#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Servo.h>
#include <ArduinoOTA.h>

#include "servoarm.js.h"


// Replace with your network credentials
const char* ssid     = "servoarm";
const char* password = "12345678";

// Set web server port number to 80
ESP8266WebServer server(80);

class controlled_servo {
  private:
    Servo servo;
  public:
    String name;
    int min = 0;
    int max = 0;
    int port;
    String orient = "vertical";
    bool attached = false;
  
    void write(int angle){
      if(!attached){
        attached = true;
        servo.attach(port);
      }
      if(angle<min){
        angle=min;
      }
      if(angle>max){
        angle=max;
      }
      servo.write(angle);
    }
};

const static int NUM_SERVOS = 4;

controlled_servo servos[NUM_SERVOS];

void SetupServos(){
  servos[0].name = "Rotation";
  servos[0].min = 0;
  servos[0].max = 180;
  servos[0].port = D0;
  servos[0].orient = "horizontal";
  servos[1].name = "Height";
  servos[1].min = 90;
  servos[1].max = 180;
  servos[1].port = D1;
  servos[2].name = "Distance";
  servos[2].min = 0;
  servos[2].max = 180;
  servos[2].port = D2;
  servos[3].name = "Claw";
  servos[3].min = 80;
  servos[3].max = 180;
  servos[3].port = D3;
}

String GetLimitsPage(){
  String page = "";
  page += "<html><form><table border=1>";
  page += "<tr><th>Label</th><th>Min</th><th>Max</td></tr>";
  for(int i = 0; i < NUM_SERVOS; i++){
    char buf[200];
    sprintf(buf, "<tr><td>%s</td><td><input type=number name=%s value=%d></td><td><input type=number name=%s value=%d></td></tr>",
    (servos[i].name + "_min").c_str(), servos[i].min, (servos[i].name + "_max").c_str(), servos[i].max);
    page += buf;
  }
  
  page += "</table>";
  page += "<input type=submit value=submit>";
  page += "</form></html>";
  return page;
}

String GetControlsPage(){
  String page = "";
  page += "<html><body scroll=\"no\" style=\"overflow: hidden\">";
  for(int i = 0; i < NUM_SERVOS; i++){
    char buf[200];
    sprintf(buf, "<input type=range id=%s min=%d max=%d orient=%s value=%d onchange=\"UpdatePositionsSlider()\">",
    servos[i].name.c_str(), servos[i].min, servos[i].max, servos[i].orient.c_str(), (servos[i].min + servos[i].max)/2);
    page += buf;
  }
  page += "</body>";
  page += servoarm;
  page += "</html>";
  return page;
}

void SetControls(){
  for (int i = 0; i < server.args(); i++) {
    for(int j = 0; j < NUM_SERVOS; j++){
      if(server.argName(i) == servos[j].name){
         servos[j].write(server.arg(i).toInt());
         break;
      }
    }
  }
}
bool ota = false;
void OtaUpdate(){
  ArduinoOTA.begin();
  ota = true;
}

void setup() {
  WiFi.softAP(ssid, password, 10, 1);
  SetupServos();
  server.on("/ota", [](){
    server.send(200, "text/html", "OTA Update Ready");
    OtaUpdate();
  });
  server.on("/limits", [](){
    server.send(200, "text/html", GetLimitsPage());
  });
  server.on("/control", [](){
    server.send(200, "text/html", "Sent");
    SetControls();
  });
  server.on("/", [](){
    server.send(200, "text/html", GetControlsPage());
  });
  
  server.begin();
  
}

void loop() {
  if (ota){
    ArduinoOTA.handle();
    return;
  }
  // put your main code here, to run repeatedly:
  server.handleClient();
}
