Code that controls a simple ESP8266 Servo Arm.

This is the hardware I wrote the code for. It 
is a 3D Printed servo arm using the simple and
cheap sg90 hobby servos.

![My Hardware](hardware.JPG)